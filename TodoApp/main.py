from fastapi import FastAPI
from TodoApp.api.endpoints import auth, user , todo , admin , address
from TodoApp.core.session import Base
from TodoApp.core.session import engine

app = FastAPI()

Base.metadata.create_all(bind=engine)


app.include_router(auth.router, prefix="/auth", tags=["auth"])
app.include_router(user.router, prefix="/user", tags=["user"])
app.include_router(todo.router, prefix="/todos", tags=["todos"])
app.include_router(admin.router, prefix="/admin", tags=["admin"])
app.include_router(address.router, prefix="/address", tags=["address"])
