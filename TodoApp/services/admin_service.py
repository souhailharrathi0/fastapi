from http.client import HTTPException

from sqlalchemy.orm import Session
from TodoApp.models.user import Users
from TodoApp.models.todo import Todo


def read_all_todos(db: Session, user: Users):
    """
    Fetches all todos if the user has an admin role.
    """
    if user is None or user.role != 'admin':  # Adjust based on your Users model's attribute
        raise HTTPException(status_code=401, detail='Authentication Failed')
    return db.query(Todo).all()


def read_all_users(db: Session, user: Users):
    """
    Fetches all users if the user has an admin role.
    """
    if user is None or user.role != 'admin':  # Adjust based on your Users model's attribute
        raise HTTPException(status_code=401, detail='Authentication Failed')
    return db.query(Users).all()
