from sqlalchemy.orm import Session
from TodoApp.models.user import Users
from TodoApp.models.address import Address
from TodoApp.schemas.address_schema import AddressCreate


def create_address(db: Session, address_create: AddressCreate, user_id: int):
    """
    Create a new address and update the user's address_id.

    :param db: Database session.
    :param address_create: Address creation data.
    :param user_id: ID of the user to associate the address with.
    :return: The created Address instance.
    """
    # Create a new Address instance
    address = Address(
        address1=address_create.address1,
        address2=address_create.address2,
        city=address_create.city,
        state=address_create.state,
        country=address_create.country,
        postalcode=address_create.postalcode
    )

    db.add(address)
    db.commit()
    db.refresh(address)

    # Fetch the user instance by ID and update address_id
    user = db.query(Users).filter(Users.id == user_id).first()
    if not user:
        raise Exception("User not found")  # Consider using a more specific exception
    user.address_id = address.id

    db.add(user)
    db.commit()
    return address
