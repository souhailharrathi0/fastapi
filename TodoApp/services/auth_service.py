from sqlalchemy.orm import Session
from ..models.user import Users
from ..core.security import verify_password, get_password_hash
from ..schemas.users_schema import UserCreate
from sqlalchemy.exc import IntegrityError
from fastapi import HTTPException


def authenticate_user(db: Session, username: str, password: str):
    """
    Authenticate User
    @param db:
    @param username:
    @param password:
    @return:
    """
    user = db.query(Users).filter(Users.username == username).first()
    if not user or not verify_password(password, user.hashed_password):
        return False
    return user


def create_user(db: Session, user: UserCreate):
    """
    Create a user
    @param db:
    @param user:
    @return:  new user
    """
    hashed_password = get_password_hash(user.password)
    db_user = Users(
        username=user.username,
        email=user.email,
        first_name=user.first_name,
        last_name=user.last_name,
        role=user.role,
        phone_number=user.phone_number,
        hashed_password=hashed_password
    )
    try:
        db.add(db_user)
        db.commit()
        db.refresh(db_user)
        return db_user
    except IntegrityError:
        db.rollback()
        raise HTTPException(status_code=400, detail="Username or email already registered")
