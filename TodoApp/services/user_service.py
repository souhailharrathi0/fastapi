from sqlalchemy.orm import Session
from TodoApp.models.user import Users
from TodoApp.core.security import get_password_hash, verify_password


def get_user_by_username(db: Session, username: str) -> Users:
    """
    Get a User by username (unique)
    @param db:
    @param username:
    @return: User
    """
    return db.query(Users).filter(Users.username == username).first()


def change_user_password(db: Session, user: Users, new_password: str):
    """
    Change User password
    @param db:
    @param user:
    @param new_password:
    @return: None
    """
    hashed_password = get_password_hash(new_password)
    user.hashed_password = hashed_password
    db.commit()


def change_user_phone_number(db: Session, user: Users, new_phone_number: str):
    """
    Change User phone Number
    @param db:
    @param user:
    @param new_phone_number:
    @return: None
    """
    user.phone_number = new_phone_number
    db.commit()
