from sqlalchemy.orm import Session
from TodoApp.models.todo import Todo
from fastapi import HTTPException


def get_todos_for_user(db: Session, user_id: int):
    """
    Get All todos from a user
    @param db:
    @param user_id:
    @return:
    """
    return db.query(Todo).filter(Todo.owner_id == user_id).all()


def get_todo_by_id_for_user(db: Session, todo_id: int, user_id: int):
    """
     Get a _todo by it is ID  from a user
     @param db:
     @param user_id:
     @return: A _Todo
     """
    return db.query(Todo).filter(Todo.id == todo_id, Todo.owner_id == user_id).first()


def create_todo_for_user(db: Session, todo_data: dict, user_id: int) -> Todo:
    """
    Create a new_todo
    @param db:
    @param todo_data:
    @param user_id:
    @return: new_todo ( the created _todo)
    """
    new_todo = Todo(**todo_data, owner_id=user_id)
    db.add(new_todo)
    db.commit()
    db.refresh(new_todo)
    return new_todo


def delete_todo_by_id_for_user(db: Session, todo_id: int, user_id: int):
    """
    Find the _todo item by ID and owner ID
    @param db:
    @param todo_id:
    @param user_id:
    """
    todo_item = db.query(Todo).filter(Todo.id == todo_id, Todo.owner_id == user_id).first()

    if todo_item:
        db.delete(todo_item)
        db.commit()
    else:
        # If the _todo item does not exist or does not belong to the user, raise an exception
        raise HTTPException(status_code=404, detail="Todo not found or not owned by this user")


def update_todo_by_id_for_user(db: Session, todo_id: int, user_id: int, todo_data: dict):
    """
    Find the _todo item by ID and owner ID
    @param db:
    @param todo_id:
    @param user_id:
    @param todo_data:
    @return:
    """
    todo_item = db.query(Todo).filter(Todo.id == todo_id, Todo.owner_id == user_id).first()

    if todo_item:
        # Update the _todo item with the new data
        for key, value in todo_data.items():
            setattr(todo_item, key, value)
        db.commit()
        return todo_item
    else:
        # If the _todo item does not exist or does not belong to the user, raise an exception
        raise HTTPException(status_code=404, detail="Todo not found or not owned by user")
