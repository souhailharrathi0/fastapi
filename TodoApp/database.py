from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# change production database to be able to run with docker
SQLALCHEMY_DATABASE_URL = 'postgresql://postgres:test123@db/TodoAppDatabase'
SQLALCHEMY_DATABASE_URL_TEST = 'sqlite:///./testdb.db'

engine = create_engine(SQLALCHEMY_DATABASE_URL)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

