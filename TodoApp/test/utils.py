import pytest
from sqlalchemy import text
from fastapi.testclient import TestClient
from ..core.security import pwd_context
from ..models.user import Users
from ..models.todo import Todo
from ..core.session import Base, test_engine, TestingSessionLocal
from ..main import app

Base.metadata.create_all(bind=test_engine)

client = TestClient(app)


@pytest.fixture
def test_user():
    user = Users(
        username="codingwithrobytest",
        email="codingwithrobytest@email.com",
        first_name="Eric",
        last_name="Roby",
        hashed_password=pwd_context.hash("testpassword"),
        role="admin",
        phone_number="(111)-111-1111",

    )
    db = TestingSessionLocal()
    db.add(user)
    db.commit()
    yield user
    with test_engine.connect() as connection:
        connection.execute(text("DELETE FROM users;"))


@pytest.fixture
def test_todo(test_user):
    todo = Todo(
        title="Learn to code!",
        description="Need to learn everyday!",
        priority=5,
        complete=False,
        owner_id=test_user.id,
    )

    db = TestingSessionLocal()
    db.add(todo)
    db.commit()
    yield todo
    with test_engine.connect() as connection:
        connection.execute(text("DELETE FROM todos;"))
