import pytest


class Student:
    def __init__(self, firstname: str, lastname: str, major: str, years: int):
        self.firstname = firstname
        self.lastname = lastname
        self.major = major
        self.years = years


@pytest.fixture
def default_employee():
    return Student('Jhon', 'Doe', 'Computer Science', 3)


def test_person_initialization(default_employee):
    assert default_employee.firstname == 'Jhon', 'First name should be Jhon'
    assert default_employee.lastname == 'Doe', 'Last name should be Jhon'
    assert default_employee.major == 'Computer Science'
    assert default_employee.years == 3
