from .utils import *
from fastapi import status
from ..api.deps import get_db, get_current_user, override_get_current_user, override_get_db

app.dependency_overrides[get_db] = override_get_db
app.dependency_overrides[get_current_user] = override_get_current_user


def test_read_current_user(test_user):
    """
    Tessting our def for reading current user
    @param test_user:
    """
    response = client.get("/user/me")
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['username'] == 'codingwithrobytest'
    assert response.json()['email'] == 'codingwithrobytest@email.com'
    assert response.json()['first_name'] == 'Eric'
    assert response.json()['last_name'] == 'Roby'
    assert response.json()['role'] == 'admin'
    assert response.json()['phone_number'] == '(111)-111-1111'


def test_change_password_success(test_user):
    """
    Testing our def for changing password in case of success
    @param test_user:
    """
    response = client.put("/user/password", json={"password": "testpassword",
                                                  "new_password": "newpassword"})
    assert response.status_code == status.HTTP_204_NO_CONTENT


def test_change_password_invalid_current_password(test_user):
    """
    Testing our def for changing password in case of failure due to wrong password
    @param test_user:
    """
    response = client.put("/user/password", json={"password": "wrong_password",
                                                  "new_password": "newpassword"})
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json() == {'detail': 'Incorrect current password'}


def test_change_phone_number(test_user):
    """
    Testing our Def for changing phone number
    @param test_user:
    @return:
    """
    response = client.put("/user/phone_number/", json={"new_phone_number": "12345678900"})

    assert response.status_code == status.HTTP_204_NO_CONTENT
