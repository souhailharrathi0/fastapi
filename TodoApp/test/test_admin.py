from .utils import *
from fastapi import status
from ..api.deps import get_db, get_current_user, override_get_current_user, override_get_db

app.dependency_overrides[get_db] = override_get_db
app.dependency_overrides[get_current_user] = override_get_current_user


def test_read_all_todos_endpoint(test_todo):
    """
    Testing our def for reading all todos
    @param test_todo:
    """
    response = client.get("/admin/todos")
    todos = response.json()
    todo = todos[0]
    assert todo["title"] == "Learn to code!"
    assert todo["description"] == "Need to learn everyday!"
    assert todo["priority"] == 5
    assert not todo["complete"]
