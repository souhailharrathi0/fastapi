from fastapi import APIRouter, Depends, HTTPException, status, Body
from sqlalchemy.orm import Session
from TodoApp.api.deps import get_current_user, get_db
from TodoApp.core.security import verify_password
from TodoApp.models.user import Users
from TodoApp.schemas.users_schema import UserVerification, UserPhoneUpdate, UserInDB
from TodoApp.services.user_service import change_user_password, change_user_phone_number, get_user_by_username
from fastapi.responses import Response
from ..deps import user_dependency, db_dependency

router = APIRouter()


@router.get("/me", response_model=UserInDB)
async def read_current_user(current_user: UserInDB = user_dependency, db: Session = db_dependency):
    """
    Router for reading current user Information
    """
    user = get_user_by_username(db, username=current_user.username)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return user


@router.put("/password", status_code=status.HTTP_204_NO_CONTENT)
async def update_password(
        password_update: UserVerification = Body(...),
        current_user: Users = user_dependency,
        db: Session = db_dependency):
    """
    Router for Updating Password

    """
    if not verify_password(password_update.password, current_user.hashed_password):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect current password")

    change_user_password(db, current_user, password_update.new_password)


@router.put("/phone_number", status_code=status.HTTP_204_NO_CONTENT)
async def update_phone_number(
        phone_update: UserPhoneUpdate = Body(...),
        current_user: Users = user_dependency,
        db: Session = db_dependency,
        response: Response = None):
    """
    Router for Updating Phone Number
    """
    change_user_phone_number(db, current_user, phone_update.new_phone_number)
    response.headers["X-Status"] = "Phone number updated successfully"  # Set custom header
    return Response(status_code=status.HTTP_204_NO_CONTENT)
