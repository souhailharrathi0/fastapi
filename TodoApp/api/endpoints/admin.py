# TodoApp/api/routers/admin_router.py
from starlette import status
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from TodoApp.api.deps import get_current_user, get_db
from TodoApp.models.user import Users
from TodoApp.services.admin_service import read_all_todos, read_all_users

router = APIRouter()


@router.get("/todos", status_code=status.HTTP_200_OK)
async def read_all_todos_endpoint(
        current_user: Users = Depends(get_current_user),
        db: Session = Depends(get_db)):
    """
    Endpoint to read all todos for admins.
    """
    todos = read_all_todos(db, current_user)
    return todos


@router.get("/users", status_code=status.HTTP_200_OK)
async def read_all_users_endpoint(
        current_user: Users = Depends(get_current_user),
        db: Session = Depends(get_db)):
    """
    Endpoint to read all users for admins.
    """
    users = read_all_users(db, current_user)
    return users
