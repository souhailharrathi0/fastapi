from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from TodoApp.core.security import create_access_token
from TodoApp.schemas.users_schema import UserCreate, UserInDB
from TodoApp.services.auth_service import authenticate_user, create_user
from fastapi.security import OAuth2PasswordRequestForm
from ..deps import db_dependency

router = APIRouter()


@router.post("/signup", response_model=UserInDB)
async def signup(user: UserCreate, db: Session = db_dependency):
    """
    Router for Creating A User
    """
    return create_user(db, user)


@router.post("/login")
async def login(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = db_dependency):
    """
    Router for Login

    """
    user = authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(status_code=400, detail="Incorrect username or password")
    access_token = create_access_token(data={"sub": user.username})
    return {"access_token": access_token, "token_type": "bearer"}
