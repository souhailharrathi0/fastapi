# TodoApp/api/routers/admin_router.py
from starlette import status
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from TodoApp.api.deps import get_current_user, get_db, db_dependency, user_dependency
from TodoApp.models.user import Users
from TodoApp.schemas.address_schema import AddressCreate
from TodoApp.schemas.users_schema import UserInDB
from TodoApp.services.address_service import create_address

router = APIRouter()


@router.post("/users/{user_id}/create",status_code=status.HTTP_201_CREATED)
def create_user_address(current_user: UserInDB = user_dependency, db: Session = db_dependency,
                        address_in: AddressCreate = Depends()):
    """
    Create an address for a user.
    """
    try:
        address = create_address(db=db, address_create=address_in, user_id=current_user.id)
        return address
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))
