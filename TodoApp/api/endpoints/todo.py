from fastapi import APIRouter, Depends, HTTPException, Path
from sqlalchemy.orm import Session
from starlette import status
from TodoApp.models.user import Users
from TodoApp.schemas.todos_schema import TodosRequest
from TodoApp.services.todo_service import get_todos_for_user, create_todo_for_user, get_todo_by_id_for_user, \
    delete_todo_by_id_for_user, update_todo_by_id_for_user
from ..deps import user_dependency, db_dependency

router = APIRouter()


@router.get("/get all", status_code=status.HTTP_200_OK)
async def read_todos_for_current_user(
        db: Session = db_dependency,
        current_user: Users = user_dependency):
    todos = get_todos_for_user(db, user_id=current_user.id)
    if not todos:
        raise HTTPException(status_code=404, detail="Todos not found")
    return todos


@router.get("/todo/{todo_id}", status_code=status.HTTP_200_OK)
async def read_todo(
        todo_id: int = Path(gt=0),
        current_user: Users = user_dependency,
        db: Session = db_dependency):
    """
    Read a specific _todo by its ID, ensuring that only the owner can access it.
    """
    if current_user is None:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail='Authentication failed')

    todo_model = get_todo_by_id_for_user(db=db, todo_id=todo_id, user_id=current_user.id)

    if todo_model is not None:
        return todo_model
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Todo not found")


@router.post("/create", status_code=status.HTTP_201_CREATED)
async def create_todo(
        current_user: Users = user_dependency,
        db: Session = db_dependency,
        todo_request: TodosRequest = Depends()):
    """
    Create a new _todo item for the current user.
    """
    # user dependency will ensure user is authenticated and return the user model
    if current_user is None:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail='Authentication failed')

    # Use the service to create a new _todo item
    new_todo = create_todo_for_user(db=db, todo_data=todo_request.dict(), user_id=current_user.id)
    return new_todo


@router.put("/todo/{todo_id}", status_code=status.HTTP_204_NO_CONTENT)
async def update_todo(
        todo_id: int = Path(gt=0),
        todo_request: TodosRequest = Depends(),  # Using Depends() to include Pydantic validation
        current_user: Users = user_dependency,  # this returns the current authenticated user model
        db: Session = db_dependency):
    """
    Update a specific _todo item by its ID. This endpoint ensures that only the owner can update their _todo.
    """
    # Extract the user ID from the user dependency
    user_id = current_user.id

    # Call the service function to update the _todo
    try:
        update_todo_by_id_for_user(db=db, todo_id=todo_id, user_id=user_id, todo_data=todo_request.dict())
    except HTTPException as e:
        # Re-raise the HTTPException from the service layer
        raise e
    except Exception as e:
        # Catch any other unexpected errors and raise a 500 internal server error
        raise HTTPException(status_code=500, detail=str(e))


@router.delete("/todo/{todo_id}", status_code=204)
async def delete_todo(
        todo_id: int = Path(gt=0),
        current_user: Users = user_dependency,
        db: Session = db_dependency):
    """
    Delete a specific _todo by its ID, ensuring that only the owner can delete it.
    """
    delete_todo_by_id_for_user(db=db, todo_id=todo_id, user_id=current_user.id)
    return {"detail": "Todo deleted successfully"}
