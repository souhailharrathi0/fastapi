from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session
from jose import jwt, JWTError

from TodoApp.core.config import settings
from TodoApp.core.security import pwd_context
from TodoApp.core.session import SessionLocal, TestingSessionLocal  # Ensure this import matches your project structure
from TodoApp.models.user import Users

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth/login")


def get_db() -> Session:
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


async def get_current_user(db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)) -> Users:
    """
    Get the current user connected to the app
    @param db:
    @param token:
    @return: current cuser
    """
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    try:
        payload = jwt.decode(token, settings.secret_key, algorithms=[settings.algorithm])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        # Directly use username from payload to query the database.
        user = db.query(Users).filter(Users.username == username).first()
        if user is None:
            raise credentials_exception
        return user
    except JWTError:
        raise credentials_exception


db_dependency = Depends(get_db)
user_dependency = Depends(get_current_user)


# deps for testing

def override_get_db():
    db = TestingSessionLocal()
    try:
        yield db
    finally:
        db.close()


class FakeUser:
    def __init__(self, **kwargs):
        self.hashed_password = pwd_context.hash("testpassword")
        for key, value in kwargs.items():
            setattr(self, key, value)


def override_get_current_user():
    return FakeUser(username='codingwithrobytest', email='codingwithrobytest@email.com', first_name='Eric',
                    last_name='Roby', id=1, is_active=True, phone_number='12345655555', role='admin')
