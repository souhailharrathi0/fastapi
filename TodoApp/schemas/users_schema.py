from pydantic import BaseModel, Field


class UserCreate(BaseModel):
    username: str
    email: str
    first_name: str
    last_name: str
    password: str
    role: str
    phone_number: str


class UserVerification(BaseModel):
    password: str
    new_password: str = Field(..., min_length=6)


class UserPhoneUpdate(BaseModel):
    new_phone_number: str = Field(..., min_length=10, example="12345678900")


class Token(BaseModel):
    access_token: str
    token_type: str


class UserBase(BaseModel):
    username: str
    email: str
    first_name: str
    last_name: str
    role: str
    phone_number: str


class UserInDB(UserBase):
    id: int
    is_active: bool

    class Config:
        from_attributes = True  # Updated for Pydantic v2
