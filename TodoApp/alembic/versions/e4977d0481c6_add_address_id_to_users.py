"""Add address_id to users

Revision ID: e4977d0481c6
Revises: 
Create Date: 2024-03-30 17:58:31.523725

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'e4977d0481c6'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # Add the address_id column to the users table
    op.add_column('users', sa.Column('address_id', sa.Integer(), nullable=True))
    # Add a foreign key constraint to the address_id column
    op.create_foreign_key('fk_users_address', source_table='users', referent_table='address',
                          local_cols=['address_id'], remote_cols=['id'], ondelete='CASCADE')


def downgrade():
    # Remove the foreign key constraint
    op.drop_constraint('fk_users_address', 'users', type_='foreignkey')
    # Remove the address_id column
    op.drop_column('users', 'address_id')
