"""add apt num col on address

Revision ID: bdb194320d36
Revises: 39b6090f1a34
Create Date: 2024-03-23 00:21:02.060901

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'bdb194320d36'
down_revision: Union[str, None] = '39b6090f1a34'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column('address', sa.Column('apt_num', sa.Integer(), nullable=True))


def downgrade() -> None:
    op.drop_column('address', 'apt_num')
