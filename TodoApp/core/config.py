from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    """
    My settings for my project
    """
    database_url: str = "postgresql://postgres:test123@localhost/fastapiv1"
    secret_key: str = "VERYSECRETKEY"
    algorithm: str = "HS256"
    access_token_expire_minutes: int = 30
    test_database_url: str = "postgresql://postgres:test123@localhost/fastapiv1test"


settings = Settings()
