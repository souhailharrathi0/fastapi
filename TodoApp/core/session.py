from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from .config import settings

engine = create_engine(settings.database_url)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

test_engine = create_engine(settings.test_database_url)

TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=test_engine)
